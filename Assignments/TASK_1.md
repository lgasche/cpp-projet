# Gestion mémoire

## Analyse de la gestion des avions

La création des avions est aujourd'hui gérée par les fonctions `TowerSimulation::create_aircraft` et `TowerSimulation::create_random_aircraft`.
Chaque avion créé est ensuite placé dans les files `GL::display_queue` et `GL::move_queue`.

Si à un moment quelconque du programme, vous souhaitiez accéder à l'avion ayant le numéro de vol "AF1250", que devriez-vous faire ?

```
Il faut faire une recherche dans une des queues : move_queue ou display_queue.
```

---

## Objectif 1 - Référencement des avions

### A - Choisir l'architecture

Pour trouver un avion particulier dans le programme, ce serait pratique d'avoir une classe qui référence tous les avions et qui peut donc nous renvoyer celui qui nous intéresse.

Vous avez 2 choix possibles :
- créer une nouvelle classe, `AircraftManager`, qui assumera ce rôle,
- donner ce rôle à une classe existante.

Réfléchissez aux pour et contre de chacune de ces options.

```
AircraftManager : L'ajout d'une nouvelle classe pour remplire ce role, respecte totalement le principe de la poo, une classe == une résponsabilité, cependant cela nous oblige de gérer un nouveau objet dans tous le programme qui est déjà complexe.

Dans une classe existance, cela viole le principe cité plus haut, mais cela peut rendre l'implémentation de cette ajout plus rapide actuellement.
```

Pour le restant de l'exercice, vous partirez sur le premier choix.

### B - Déterminer le propriétaire de chaque avion

Vous allez introduire une nouvelle liste de références sur les avions du programme.
Il serait donc bon de savoir qui est censé détruire les avions du programme, afin de déterminer comment vous allez pouvoir mettre à jour votre gestionnaire d'avions lorsque l'un d'entre eux disparaît.

Répondez aux questions suivantes :
1. Qui est responsable de détruire les avions du programme ? (si vous ne trouvez pas, faites/continuez la question 4 dans TASK_0)
```
Actuellement cela est gérer dans opengl_interface.cpp, fonction timer, via la move_queue.
```
2. Quelles autres structures contiennent une référence sur un avion au moment où il doit être détruit ?
```
La GL::display_queue posséde une réfèrence dessus (une suppression est aussi effectuer dedans).
```
3. Comment fait-on pour supprimer la référence sur un avion qui va être détruit dans ces structures ?
```
Chaque dynamic_object possèdent une fonction must_destroyed() qui return false, sauf pour les avions, si ceux-ci respectent certainent conditions : if (visited_a_terminal == true && is_on_ground() == false && waypoints.empty())

On effectue donc une recherche pour les identifiers, puis ils sont supprimer manuellement.
```
4. Pourquoi n'est-il pas très judicieux d'essayer d'appliquer la même chose pour votre `AircraftManager` ?
```
Il semble que le but d'AircraftManager est de gérer les avions totalement, de leurs créations à la destruction.
Donc implémenté la gestion de la même manière que précédement dans AircraftManager serait "bizzare".
```

Pour simplifier le problème, vous allez déplacer l'ownership des avions dans la classe `AircraftManager`.
Vous allez également faire en sorte que ce soit cette classe qui s'occupe de déplacer les avions, et non plus la fonction `timer`.

### C - C'est parti !

Ajoutez un attribut `aircrafts` dans le gestionnaire d'avions.
Choisissez un type qui met bien en avant le fait que `AircraftManager` est propriétaire des avions.
```cpp
std::unique_ptr<Aircraft>
```

Ajoutez un nouvel attribut `aircraft_manager` dans la classe `TowerSimulation`.
```cpp
tower_sim.hpp
AircraftManager aircraft_manager;

tower_sim.cpp
aircraft_manager = AircraftManager();
```

Faites ce qu'il faut pour que le `AircraftManager` puisse appartenir à la liste `move_queue`.
``` cpp
inline std::unordered_set<DynamicObject*> move_queue;

class AircraftManager : public GL::DynamicObject
```
```
Il faut donc que AircraftManager hérite de GL::DynamicObject.
```

Ajoutez la fonction appropriée dans `AircraftManager` pour demander de bouger (`move`) les avions.
```cpp
void move() override;

void AircraftManager::move()
{
    for (auto it = aircrafts.begin(); it != aircrafts.end();)
    {
        (*it)->move();
        // Destroy aircraft after take-off
        if ((*it)->must_destroyed())
        {
            //delete *it;
            it = aircrafts.erase(it);
        }
        else
        {
            it++;
        }
   
```

Supprimez les ajouts d'`Aircraft` dans la `move_queue`.
```cpp
void TowerSimulation::create_aircraft(const AircraftType& type) const
{
    ...
    //GL::move_queue.emplace(aircraft);
    std::unique_ptr<Aircraft> aircraft =
        std::make_unique<Aircraft>(type, flight_number, start, direction, airport->get_tower());
    aircraft_manager.add_aircraft(std::move(aircraft));
}
```

En effet, ce n'est plus `timer` qui est responsable de déplacer les avions mais l'`AircraftManager`.
```cpp
void AircraftManager::add_aircraft(std::unique_ptr<Aircraft> aircraft)
{
    aircrafts.emplace_back(std::move(aircraft));
}
```

Faites le nécessaire pour que le gestionnaire supprime les avions après qu'ils soient partis de l'aéroport.

Enfin, faites ce qu'il faut pour que `create_aircraft` donne l'avion qu'elle crée au gestionnaire.
Testez que le programme fonctionne toujours.

```
Ne pas oublié d'ajouter les nouveaux files : .cpp .hpp; au CMake.

L'aircraft_manager devient un objet de la TowerSimulation, il faut donc retirer les const des fonctions : 
    void create_aircraft(const AircraftType& type);
    void create_random_aircraft();
    void create_keystrokes();
Car l'on modifie maintenant.
```

---

## Objectif 2 - Usine à avions

### A - Création d'une factory

La création des avions est faite à partir des composants suivants :
- `create_aircraft`
- `create_random_aircraft`
- `airlines`
- `aircraft_types`.

Pour éviter l'usage de variables globales, vous allez créer une classe `AircraftFactory` dont le rôle est de créer des avions.
```
On commence par déplacer creat_aircraft dans AircraftFactory.
creat_aircraft doit donc changer de signature, celle-ci ne dois plus retourner void mais un unique_ptr<Aicraft>, afin que celui-ci soit ajouter au aircraft_manager.
On retire donc la partie avec l'aircraft_manager de la fonction creat_aircraft.

Trois autres problèmes ce posent à nous dans creat_aircraft, afin crée un nouveau aicraft le constructeur à besoin :
1) AircraftType& type -> Originalement donné en parametre 
2) Tower& control_
3) airlines -> N'est pas définit dans AicraftFactory
Une possibilité est de générer le type d'aicraft dans creat_aircraft.
Pour la Tower, nous pouvons la passer en parametre de la fonction, attention il faut cependant être sur que l'airport est initialiser avant l'appel à create_aircraft : create_aircraft(airport->get_tower()) .
airlines peut être déplacer dans AircraftFactory.hpp .

```

Définissez cette classe, instanciez-là en tant que membre de `TowerSimulation` et refactorisez-le code pour l'utiliser.
Vous devriez constater que le programme crashe.

En effet, pour que la factory fonctionne, il faut que le `MediaPath` (avec la fonction `MediaPath::initialize`) et que `glut` (avec la fonction `init_gl()`) aient été initialisés.
Comme ces appels sont faits depuis le corps du constructeur de `TowerSimulation`, ils sont actuellement exécutés après la construction de la factory.
Afin de faire en sorte que les appels aient lieu dans le bon ordre, vous allez créer une structure `ContextInitializer` dans le fichier `tower_sim.hpp`.
Vous lui ajouterez un constructeur dont le rôle sera d'appeler les fonctions d'initialisation de `MediaPath`, `glut` et `srand`.

Vous pouvez maintenant ajoutez un attribut `context_initializer` de type `ContextInitializer` dans la classe `TowerSimulation`.
A quelle ligne faut-il définir `context_initializer` dans `TowerSimulation` pour s'assurer que le constructeur de `context_initializer` est appelé avant celui de `factory` ?

Refactorisez le restant du code pour utiliser votre factory.
Vous devriez du coup pouvoir supprimer les variables globales `airlines` et `aircraft_types`.
```
airlines, appartient maintenant à AircraftFactory.hpp .

aircraft_types, est maintenant calculer uniquement dans la methode create_aircraft.
```

### B - Conflits

Il est rare, mais possible, que deux avions soient créés avec le même numéro de vol.
Ajoutez un conteneur dans votre classe `AircraftFactory` contenant tous les numéros de vol déjà utilisés.
Faites maintenant en sorte qu'il ne soit plus possible de créer deux fois un avion avec le même numéro de vol.
```
Dans AircraftFactory.hpp, on rajoute un conteneur de type : unordered_set, afin de garantir un acces en temps constant (random), ce conteneur va nous permettre de garde l'intégraliter des numéros de vol déjà utilisés.
```
```cpp
std::unordered_set<std::string> flight_numbers;
```
```
Puis l'on créer une nouvelle methode : std::string new_flight_numbers();
Qui génére un nouveau nouveau numéro de vol, qui n'est pas présent dans flight_numbers.
```

### C - Data-driven AircraftType (optionnel)

On aimerait pouvoir charger les paramètres des avions depuis un fichier.

Définissez un format de fichier qui contiendrait les informations de chaque `AircraftType` disponible dans le programme.\
Ajoutez une fonction `AircraftFactory::LoadTypes(const MediaPath&)` permettant de charger ce fichier.
Les anciens `AircraftTypes` sont supprimés.

Modifiez ensuite le `main`, afin de permettre à l'utilisateur de passer le chemin de ce fichier via les paramètres du programme.
S'il ne le fait pas, on utilise la liste de type par défaut.

Si vous voulez de nouveaux sprites, vous pouvez en trouver sur [cette page](http://www.as-st.com/ttd/planes/planes.html)
(un peu de retouche par GIMP est necessaire)

---

## Objectif 3 - Pool de textures (optionnel)

Pour le moment, chacun des `AircraftType` contient et charge ses propres sprites.
On pourrait néanmoins avoir différents `AircraftType` qui utilisent les mêmes sprites.
Ils seraient donc chargés plusieurs fois depuis le disque pour rien.

Pour rendre le programme un peu plus performant, implémentez une classe `TexturePool` qui s'occupe de charger, stocker et fournir les textures.
Pour exprimer correctement ce type d'ownership, vous devez utiliser le smart-pointer `std::shared_ptr`.

Commencez par aller sur la documentation de `std::shared_ptr`.
Pouvez-vous expliquer comment le compilateur arrive à déterminer à quel moment l'objet n'a plus aucun owner, afin de le détruire ?

Listez les classes qui ont besoin de `TexturePool`.
Sachant que vous n'aurez qu'une seule instance de `TexturePool` dans votre programme, quelle classe devra assumer l'ownership de cet objet ?\
Instanciez `TexturePool` au bon endroit et refactorisez le code afin que tous les chargements de textures utilisent ce nouvel objet.
Assurez-vous que votre implémentation ne génère pas des fuites de mémoire au moment de sa destruction.
