# Se familiariser avec l'existant

## A- Exécution

Compilez et lancez le programme.

Allez dans le fichier `tower_sim.cpp` et recherchez la fonction responsable de gérer les inputs du programme.

``void TowerSimulation::create_keystrokes() const`` 

Sur quelle touche faut-il appuyer pour ajouter un avion ? 

``create_keystrokes() -> touche : x`` 

Comment faire pour quitter le programme ? 

``x ou q -> GL::exit_loop();``

A quoi sert la touche 'F' ? 

``GL::toggle_fullscreen()`` 

Ajoutez un avion à la simulation et attendez.
Que est le comportement de l'avion ?

``Il vole, cherche un terminal libre, va au terminal, puis redécolle.`` 

Quelles informations s'affichent dans la console ?

``
KL9492 is now landing...
now servicing KL9492...
done servicing KL9492
KL9492 lift off
``

Ajoutez maintenant quatre avions d'un coup dans la simulation.
Que fait chacun des avions ?

``
Les quatres avions cherche un terminal libre, si il n'y a pas de place libre, ils vont rester en vol, jusqu'à avoir une place libre (seulement 3 terminal disponnible simultanément).
``

## B- Analyse du code

Listez les classes du programme à la racine du dossier src/.
Pour chacune d'entre elle, expliquez ce qu'elle représente et son rôle dans le programme.

Pour les classes `Tower`, `Aircaft`, `Airport` et `Terminal`, listez leurs fonctions-membre publiques et expliquez précisément à quoi elles servent.
Réalisez ensuite un schéma présentant comment ces différentes classes intéragissent ensemble.

```
tower : sert à faire attendre un avions, jusqu'à lui trouver un terminal, ainsi qu'un chemin à suivre.
    WaypointQueue get_instructions(Aircraft& aircraft) -> calcule les coords pour dessiner l'avion qui attends en tournant en rond
    Tower::arrived_at_terminal(const Aircraft& aircraft) -> prend un avion et un terminal

aircraft : represente un avion
    const std::string& get_flight_num() -> donne l'id d'un avion
    float distance_to(const Point3D& p) -> donne la distance entre la position de l'avion et un point

    void display() -> draw l'avion
    void move() -> effectue tous les calcules pour deplacer l'avion (ainsi que les cas particulier crash, ect..), pris en compte de la vitesse

airport : represente un airport
    Tower& get_tower() -> donne la tours de controle
    void display() -> draw l'airoport
    void move() -> fait progresser le temps d'attente de l'avion dans le terminal (terminal : service_progress++)

Terminal : represente un terminal

    bool in_use() -> return true si un avion utilise ce terminal
    bool is_servicing() -> return si le service d'un avion n'est toujours pas fini
    void assign_craft(const Aircraft& aircraft) -> set l'avion qui utilise actuellement le terminal dans les champs du terminal
    void start_service(const Aircraft& aircraft) -> commence le service d'un avion, set le service_progress à 0.
    void finish_service() -> fini le service d'un avion avec ce terminal, set current avion à null
    void move() override -> fait progresser le service d'un avion
```

Quelles classes et fonctions sont impliquées dans la génération du chemin d'un avion ?

```
Les aircraft demandent au Tower des instructions:
tower : 
    WaypointQueue get_instructions(Aircraft& aircraft)

aircraft:
    void move()

Les Aircraft demandent au Tower des instructions:
    waypoints = control.get_instructions(*this);

Le Tower demande au Airport de reserver un terminal et retourner le chemin vers cette terminal:
    const auto vp = airport.reserve_terminal(aircraft);
```

Quel conteneur de la librairie standard a été choisi pour représenter le chemin ?
Expliquez les intérêts de ce choix.

```cpp
WaypointQueue = std::deque<Waypoint>;

Waypoint(const Point3D& position, const WaypointType type_ = wp_air) :
    Point3D { position }, type { type_ }
{}
```
```
Le choix c'est porter sur une queue, on aurait pu utiliser std::queue, mais on a choisi std::deque parce qu'on veut rajouter des Waypoints à la fin et au debut de la queue.
```

## C- Bidouillons !

1) Déterminez à quel endroit du code sont définies les vitesses maximales et accélération de chaque avion.
Le Concorde est censé pouvoir voler plus vite que les autres avions.
Modifiez le programme pour tenir compte de cela.

```
Il faut changer dans le fichier aircraft_type, me champs max_speed pour le concord.
float max_air_speed_
```

```cpp
aircraft_types[2] = new AircraftType { .02f, .1f, .02f, MediaPath { "concorde_af.png" } };
//Le 2eme champs corresponds à max_air_speed_
```


2) Identifiez quelle variable contrôle le framerate de la simulation.
Ajoutez deux nouveaux inputs au programme permettant d'augmenter ou de diminuer cette valeur.
Essayez maintenant de mettre en pause le programme en manipulant ce framerate. Que se passe-t-il ?\
Ajoutez une nouvelle fonctionnalité au programme pour mettre le programme en pause, et qui ne passe pas par le framerate.

```
dossier : GL -> opengl_interface.cpp -> fonction timer 

Dans le fichier "tower_sim" on rajouter les 3 inputs.
p: pause            -> 
m: more framerate   -> void more_framerate()
l: less framerate   -> void less_framerate()
Puis on définit les signatures des focntions dans opengl_interface.hpp et le code de celle-ci dans le .cpp .

Si l'on décrémente le ticks_per_sec jusqu'à ce que celui-ci soit au moins égaux à 0, alors le programme crash du a un core dumped. Car la fonction time gérant le framerate effectue une division par ticks_per_sec dans sonc code, or divisé par 0 est impossible.

La fonction pause set un bool qui définit si la pause est active ou non, et cela appel la fonction glutTimerFunc() avec son 1er parametre à 0 ou non.
```

3) Identifiez quelle variable contrôle le temps de débarquement des avions et doublez-le.

```
Dans terminal.hpp : unsigned int service_progress    = SERVICE_CYCLES; -> unsigned int service_progress    = SERVICE_CYCLES * 2;
// number of cycles needed to service an aircraft at a terminal
constexpr unsigned int SERVICE_CYCLES = 20u;
```

4) Lorsqu'un avion a décollé, il réattérit peu de temps après.
Faites en sorte qu'à la place, il soit retiré du programme.\
Indices :\
A quel endroit pouvez-vous savoir que l'avion doit être supprimé ?\
Pourquoi n'est-il pas sûr de procéder au retrait de l'avion dans cette fonction ?
A quel endroit de la callstack pourriez-vous le faire à la place ?\
Que devez-vous modifier pour transmettre l'information de la première à la seconde fonction ?

```
avion suprimer -> Aircraft::move; car c'est la fonction qui géré l'attérisage, l'arriver au terminal et le nouveau décolage.

pourquoi pas sup ici -> Car il n'y a que : inline std::unordered_set<DynamicObject*> move_queue; qui own les avions.

A quel endroit de la callstack -> GL::timer 

info 1er fonction à la 2nd fonction -> un bool
```

```cpp
void timer(const int step)
{
    if (is_pause != true)
    {
        for (auto it = move_queue.begin(); it != move_queue.end();)
        {
            (*it)->move();
            // Destroy aircraft after take-off
            if ((*it)->must_destroyed())
            {
                delete *it; // Delete the display_queue(it) aircraft.
                it = move_queue.erase(it);
            }
    ...
}
```

5) Lorsqu'un objet de type `Displayable` est créé, il faut ajouter celui-ci manuellement dans la liste des objets à afficher.
Il faut également penser à le supprimer de cette liste avant de le détruire.
Faites en sorte que l'ajout et la suppression de `display_queue` soit "automatiquement gérée" lorsqu'un `Displayable` est créé ou détruit.
Pourquoi n'est-il pas spécialement pertinent d'en faire de même pour `DynamicObject` ?
``
Pour automatiser ce procésuse, il faut déplacer l'ajout dans le constructeur et la supprésion dans le destructeur.
``
```cpp
Displayable(const float z_) : z { z_ } { display_queue.emplace_back(this); }

virtual ~Displayable()
{
    display_queue.erase(std::find(display_queue.begin(), display_queue.end(), this));
}
```

DynamicObject ??? TODO


6) La tour de contrôle a besoin de stocker pour tout `Aircraft` le `Terminal` qui lui est actuellement attribué, afin de pouvoir le libérer une fois que l'avion décolle.
Cette information est actuellement enregistrée dans un `std::vector<std::pair<const Aircraft*, size_t>>` (size_t représentant l'indice du terminal).
Cela fait que la recherche du terminal associé à un avion est réalisée en temps linéaire, par rapport au nombre total de terminaux.
Cela n'est pas grave tant que ce nombre est petit, mais pour préparer l'avenir, on aimerait bien remplacer le vector par un conteneur qui garantira des opérations efficaces, même s'il y a beaucoup de terminaux.\
Modifiez le code afin d'utiliser un conteneur STL plus adapté. Normalement, à la fin, la fonction `find_craft_and_terminal(const Aicraft&)` ne devrait plus être nécessaire.

```
On peut utiliser une unordered_map.
Unordered map is an associative container that contains key-value pairs with unique keys. Search, insertion, and removal of elements have average constant-time complexity. 

https://en.cppreference.com/w/cpp/container/unordered_map
```

## D- Théorie

1) Comment a-t-on fait pour que seule la classe `Tower` puisse réserver un terminal de l'aéroport ?
```
Il n'y a que la class Airport qui appel `void assign_craft(const Aircraft& aircraft)`, Tower est créer seulement à la création de l'Airpot.
```
```cpp
Airport(const AirportType& type_, const Point3D& pos_, const img::Image* image, const float z_ = 1.0f) :
    ...
    tower { *this }
{}
```

2) En regardant le contenu de la fonction `void Aircraft::turn(Point3D direction)`, pourquoi selon-vous ne sommes-nous pas passer par une réference ?
Pensez-vous qu'il soit possible d'éviter la copie du `Point3D` passé en paramètre ?

```
On ne passe pas par réfèrence car on applique une modification à ``direction`` avant d'incrémenter la vitesse : Point3D& cap_length(const float max_len) .

Cela me semble possible, mais il faut changer la manière de faire.
Peut être passer par une variable temporaire.
```

## E- Bonus

Le temps qui s'écoule dans la simulation dépend du framerate du programme.
La fonction move() n'utilise pas le vrai temps. Faites en sorte que si.
Par conséquent, lorsque vous augmentez le framerate, la simulation s'exécute plus rapidement, et si vous le diminuez, celle-ci s'exécute plus lentement.

Dans la plupart des jeux ou logiciels que vous utilisez, lorsque le framerate diminue, vous ne le ressentez quasiment pas (en tout cas, tant que celui-ci ne diminue pas trop).
Pour avoir ce type de résultat, les fonctions d'update prennent généralement en paramètre le temps qui s'est écoulé depuis la dernière frame, et l'utilise pour calculer le mouvement des entités.

Recherchez sur Internet comment obtenir le temps courant en C++ et arrangez-vous pour calculer le dt (delta time) qui s'écoule entre deux frames.
Lorsque le programme tourne bien, celui-ci devrait être quasiment égale à 1/framerate.
Cependant, si le programme se met à ramer et que la callback de glutTimerFunc est appelée en retard (oui oui, c'est possible), alors votre dt devrait être supérieur à 1/framerate.

Passez ensuite cette valeur à la fonction `move` des `DynamicObject`, et utilisez-la pour calculer les nouvelles positions de chaque avion.
Vérifiez maintenant en exécutant le programme que, lorsque augmentez le framerate du programme, vous n'augmentez pas la vitesse de la simulation.

Ajoutez ensuite deux nouveaux inputs permettant d'accélérer ou de ralentir la simulation.
