# Algorithmes

## Objectif 1 - Refactorisation de l'existant

### A - Structured Bindings

`TowerSimulation::display_help()` est chargé de l'affichage des touches disponibles.
Dans sa boucle, remplacez `const auto& ks_pair` par un structured binding adapté.
```cpp
    for (auto& [key, value] : GL::keystrokes)
```

### B - Algorithmes divers

1. `AircraftManager::move()` (ou bien `update()`) supprime les avions de la `move_queue` dès qu'ils sont "hors jeux".
En pratique, il y a des opportunités pour des pièges ici. Pour les éviter, `<algorithm>` met à disposition la fonction `std::remove_if`.
Remplacez votre boucle avec un appel à `std::remove_if`.
```
Les avions peuvent être retirer du vector qui les contient pour différentes raisons :
1) l'avion est hors jeu
2) l'avion ce crash car plus de carburant
3) (l'avion dissparait quand il touche le sol)

Les avions contenue dans le vector sont des unique_ptr, ils peuvent donc avoir des pointeur sur eux dans d'autre objet, tel qu'un terminal si l'avion n'a plus de carburant il se crash avant d'arriver le terminal est donc bloqué à jamais.
Il faut donc penser à supprimer tous les pointeurs sur l'avion à détruire.
```
```cpp
    const auto lambda = [](auto & aircraft){
        aircraft->move();
        return aircraft->must_destroyed();
    };

    aircrafts.erase(std::remove_if(aircrafts.begin(), aircrafts.end(), lambda), aircrafts.end());
```
```
auto est avantageux dans ce cas présent, car cela évite de préciser qu'il sagit d'une réfèrence sur un unique_ptr<Aircraft> .

Attention : remove_if est un faux amis, celui-ci ne remove rien, il va déplacer a la fin du vector tout les objets qui vérifit la lambda et renvoyer la nouvelle position de fin (dernier element non deplacer), ce qui va permettre a erase d'effacer tout les objets de cette position jusqu'à la fin du vector.
```

**Attention**: pour cela c'est necessaire que `AircraftManager` stocke les avion dans un `std::vector` ou `std::list` (c'est déjà le cas pour la solution filé).

2. Pour des raisons de statistiques, on aimerait bien être capable de compter tous les avions de chaque airline.
A cette fin, rajoutez des callbacks sur les touches `0`..`7` de manière à ce que le nombre d'avions appartenant à `airlines[x]` soit affiché en appuyant sur `x`.
Rendez-vous compte de quelle classe peut acquérir cet information. Utilisez la bonne fonction de `<algorithm>` pour obtenir le résultat.
```
Les airlines sont des données utile lors de la création d'un avion, elles ce trouve donc dans AircraftFactory.
Cependant la méthode permettant de compter le nombre d'avions présent sur une airline spécifique est dans AircraftManager, afin de respecter le principe objet : une classe une responsabilité.
```

### C - Relooking de Point3D

La classe `Point3D` présente beaucoup d'opportunités d'appliquer des algorithmes.
Particulairement, des formulations de type `x() = ...; y() = ...; z() = ...;` se remplacent par un seul appel à la bonne fonction de la librairie standard.
Remplacez le tableau `Point3D::values` par un `std::array` et puis,
remplacez le code des fonctions suivantes en utilisant des fonctions de `<algorithm>` / `<numeric>`:

1. `Point3D::operator*=(const float scalar)`
2. `Point3D::operator+=(const Point3D& other)` et `Point3D::operator-=(const Point3D& other)`
3. `Point3D::length() const`
```cpp
    // std::array<T, Size> values; -> like in chapter 9
    std::array<float, 3> values;

    // float& x() { return values[0]; }
    // float x() const { return values[0]; }
    float& x() { return std::get<0>(values); }
    float x() const { return std::get<0>(values); }
    ...
```
```
Comme vus dans le chapitre 9 du cours, il est possible d'utiliser des Classes-template et des methodes-template.
Je ne sais pas encore comment faire dans le cas de Point3D, mais je pense que cela peut être possible comme avec l'exemple mis entre commentaire.
D'autant plus que la methode get() de la std d'array est une methode-template.
```
```
Pour lenght ne pas oublié d'utiliser un float dans l'accumulateur 0.0f; sinon on déclanche un core dump.
```

---

## Objectif 2 - Rupture de kérosène

Vous allez introduire la gestion de l'essence dans votre simulation.\
Comme le but de ce TP est de vous apprendre à manipuler les algorithmes de la STL, avant d'écrire une boucle, demandez-vous du coup s'il n'existe pas une fonction d'`<algorithm>` ou de `<numeric>` qui permet de faire la même chose.

La notation tiendra compte de votre utilisation judicieuse de la librairie standard. 

### A - Consommation d'essence

Ajoutez un attribut `fuel` à `Aircraft`, et initialisez-le à la création de chaque avion avec une valeur aléatoire comprise entre `150` et `3'000`.\
Décrémentez cette valeur dans `Aircraft::update` si l'avion est en vol.\
Lorsque cette valeur atteint 0, affichez un message dans la console pour indiquer le crash, et faites en sorte que l'avion soit supprimé du manager.

N'hésitez pas à adapter la borne `150` - `3'000`, de manière à ce que des avions se crashent de temps en temps.
```
Les avions son créer avec une quantité de fuel random [150; 3000].
Dans la classe aircraft la méthode Aircraft::update va décrémenter cette quantité de fuel tant qu'elle est supérieur à 0 et que l'avion ce trouve dans les air.
La methode Aircraft::must_destroyed() gérer déjà la supprésion dans le aircraft_manager, il suffit donc de rajouter une condition qui est si l'avion est dans les airs avec un fuel egal à 0.
Il ne reste plus qu'à appeler Aircraft::update() dans Aircraft::move().
```

### B - Un terminal s'il vous plaît

Afin de minimiser les crashs, il va falloir changer la stratégie d'assignation des terminaux aux avions.
Actuellement, chaque avion interroge la tour de contrôle pour réserver un terminal dès qu'il atteint son dernier `Waypoint`.
Si un terminal est libre, la tour lui donne le chemin pour l'atteindre, sinon, elle lui demande de tourner autour de l'aéroport.
Pour pouvoir prioriser les avions avec moins d'essence, il faudrait déjà que les avions tentent de réserver un terminal tant qu'ils n'en n'ont pas (au lieu de ne demander que lorsqu'ils ont terminé leur petit tour).

1. Introduisez une fonction `bool Aircraft::has_terminal() const` qui indique si un terminal a déjà été réservé pour l'avion (vous pouvez vous servir du type de `waypoints.back()`).
2. Ajoutez une fonction `bool Aircraft::is_circling() const` qui indique si l'avion attend qu'on lui assigne un terminal pour pouvoir attérir.
3. Introduisez une fonction `WaypointQueue Tower::reserve_terminal(Aircraft& aircraft)` qui essaye de réserver un `Terminal`. Si c'est possible, alors elle retourne un chemin vers ce `Terminal`, et un chemin vide autrement (vous pouvez vous inspirer / réutiliser le code de `Tower::get_instructions`).
4. Modifiez la fonction `move()` (ou bien `update()`) de `Aircraft` afin qu'elle appelle `Tower::reserve_terminal` si l'avion est en attente. Si vous ne voyez pas comment faire, vous pouvez essayer d'implémenter ces instructions :\
\- si l'avion a terminé son service et sa course, alors on le supprime de l'aéroport (comme avant),\
\- si l'avion attend qu'on lui assigne un terminal, on appelle `Tower::reserve_terminal` et on modifie ses `waypoints` si le terminal a effectivement pu être réservé,\
\- si l'avion a terminé sa course actuelle, on appelle `Tower::get_instructions` (comme avant).
```
Gros problème qui m'a pris beaucoup de temps à résoudre :
1) Dans la methode Aircraft::move(), on appel comme demander `Tower::reserve_terminal` qui retourne un WaypointQueue.
Ce WaypointQueue contient le nouveau chemien de l'avions, il faut donc l'assigne a l'ancien.
Il ce pose donc un soucis qui est que l'opérateur d'assignation pour un WaypointQueue n'est pas définit, il faut donc le faire.
2) Une fois cette opérateur définit, dans airpot_type.hpp lorsuqu'un WaypointQueue est créer celui ci nous dit :
    implicitly-declared is deprecated
    WaypointQueue result { before_in_air, runway_middle, runway_end, crossing };
Il faut donc définir un constructeur de copie par default.
```
```cpp
    //implicitly-declared is deprecated
    Waypoint(const Waypoint&) = default;
```

### C - Minimiser les crashs

Grâce au changement précédent, dès lors qu'un terminal est libéré, il sera réservé lors du premier appel à `Aircraft::update` d'un avion recherchant un terminal.
Pour vous assurez que les terminaux seront réservés par les avions avec le moins d'essence, vous allez donc réordonner la liste des `aircrafts` avant de les mettre à jour.

Vous devrez placer au début de la liste les avions qui ont déjà réservé un terminal.\
Ainsi, ils pourront libérer leurs terminaux avant que vous mettiez à jour les avions qui essayeront de les réserver.

La suite de la liste sera ordonnée selon le niveau d'essence respectif de chaque avion.

Par exemple :
```b
A - Reserved / Fuel: 100
B - NotReserved / Fuel: 50
C - NotReserved / Fuel: 300
D - NotReserved / Fuel: 150
E - Reserved / Fuel: 2500
```
pourra être réordonné en
```b
A - Reserved / Fuel: 100
E - Reserved / Fuel: 2500
B - NotReserved / Fuel: 50
D - NotReserved / Fuel: 150
C - NotReserved / Fuel: 300
```

Assurez-vous déjà que le conteneur `AircraftManager::aircrafts` soit ordonnable (`vector`, `list`, etc).\
Au début de la fonction `AircraftManager::move` (ou `update`), ajoutez les instructions permettant de réordonner les `aircrafts` dans l'ordre défini ci-dessus.
```cpp
    // Part sorts aircrafts by booking terminal and quantity of fuel.
    const auto lambda_sort_by_less_fuel_and_terminal_reserved = [](auto& a, auto& b)
    {
        // Check by terminal reserved.
        if (a->has_terminal() != b->has_terminal())
        {
            return a->has_terminal();
        }
        // Check by quantity of fuel.
        return a->get_fuel() < b->get_fuel();
    };
    std::sort(aircrafts.begin(), aircrafts.end(), lambda_sort_by_less_fuel_and_terminal_reserved);
```

### D - Réapprovisionnement 

Afin de pouvoir repartir en toute sécurité, les avions avec moins de `200` unités d'essence doivent être réapprovisionnés par l'aéroport pendant qu'ils sont au terminal.

1. Ajoutez une fonction `bool Aircraft::is_low_on_fuel() const`, qui renvoie `true` si l'avion dispose de moins de `200` unités d'essence.\
Modifiez le code de `Terminal` afin que les avions qui n'ont pas suffisamment d'essence restent bloqués.\
Testez votre programme pour vérifier que certains avions attendent bien indéfiniment au terminal.
Si ce n'est pas le cas, essayez de faire varier la constante `200`.
```
Dans Terminal::is_servicing, il faut rajouter le check de la quantity de fuel à l'aide de Aircraft::is_low_on_fuel() .
```

2. Dans `AircraftManager`, implémentez une fonction `get_required_fuel`, qui renvoie la somme de l'essence manquante (le plein, soit `3'000`, moins la quantité courante d'essence) pour les avions vérifiant les conditions suivantes :\
\- l'avion est bientôt à court d'essence\
\- l'avion n'est pas déjà reparti de l'aéroport.
```cpp
    return std::accumulate(aircrafts.cbegin(), aircrafts.cend(), 0, lambda);
```
```
On peut utiliser std::accumulate.
```

3. Ajoutez deux attributs `fuel_stock` et `ordered_fuel` dans la classe `Airport`, que vous initialiserez à 0.\
Ajoutez également un attribut `next_refill_time`, aussi initialisé à 0.\
Enfin, faites en sorte que la classe `Airport` ait accès à votre `AircraftManager` de manière à pouvoir l'interroger.
```
Dans airport.hpp on rajoute un champs priver AircraftManager& aircraft_manager; .
Il faut donc le rajouter dans le constructeur de Airport, afin qu'il soit initialiser.

Ne pas oublié de l'ajout à l'appel du constructeur d'Airport dans tower_sim.cpp .
```

4. Ajoutez une fonction `refill` à la classe `Aircraft`, prenant un paramètre `fuel_stock` par référence non-constante.
Cette fonction rempliera le réservoir de l'avion en soustrayant ce dont il a besoin de `fuel_stock`.
Bien entendu, `fuel_stock` ne peut pas devenir négatif.\
Indiquez dans la console quel avion a été réapprovisionné ainsi que la quantité d'essence utilisée.

5. Définissez maintenant une fonction `refill_aircraft_if_needed` dans la classe `Terminal`, prenant un paramètre `fuel_stock` par référence non-constante.
Elle devra appeler la fonction `refill` sur l'avion actuellement au terminal, si celui-ci a vraiment besoin d'essence.  
```
Attention, maintenant que l'on va modifier un champs de l'Aircraft qui à reserver un terminal dans la classe Terminal.
Celui-ci (l'aircraft) ne peut plus être const, il faut donc retirer const dans le champs de Terminal et aussi dans la méthode qui l'assigne au terminal.
```

6. Modifiez la fonction `Airport::update`, afin de mettre-en-oeuvre les étapes suivantes.\
\- Si `next_refill_time` vaut 0 :\
    \* `fuel_stock` est incrémenté de la valeur de `ordered_fuel`.\
    \* `ordered_fuel` est recalculé en utilisant le minimum entre `AircraftManager::get_required_fuel()` et `5'000` (il s'agit du volume du camion citerne qui livre le kérosène).\
    \* `next_refill_time` est réinitialisé à `100`.\
    \* La quantité d'essence reçue, la quantité d'essence en stock et la nouvelle quantité d'essence commandée sont affichées dans la console.\
\- Sinon `next_refill_time` est décrémenté.\
\- Chaque terminal réapprovisionne son avion s'il doit l'être.
```
Dans la methode Terminal::refill_aircraft_if_needed(int& fuel_stock) , ne pas oublié de vérifier si le terminal à actuellement un aircraft (current_aircraft != nullptr).
Car dans Airport::update nous allons l'appeler sur chaque terminal, donc si un des terminal n'a actuellement pas d'avion assigner alors un segfault ve déclanche.
```

### E - Déréservation

Si vous avez suffisamment testé votre programme, vous avez dû vous apercevoir que parfois, certains terminaux arrêtaient d'être réservés et utilisés.\
En effet, lorsque les avions se crashent alors qu'ils avaient un terminal de réservé, rien n'a été fait pour s'assurer que le terminal allait de nouveau être libre.

Pour garantir cela, vous allez modifier le destructeur de `Aircraft`. Si l'avion a réservé un terminal, assurez-vous que celui-ci est correctement libéré. Pour cela, vous aurez besoin de rajouter une fonction dans la classe `Tower`. Choisissez-lui un nom qui décrit correctement ce qu'elle fait.
```
Dans Tower je definit une nouvelle fonction qui vas remplir le role demander : Tower::aircraft_crash_free_terminal_spot(Aircraft& aircraft) .
Un soucis c'est poser à moi, lors que l'avion effectue sa reservation (si celle-ci est approuver) sera présent dans deux partie du programme : 1) dans reserved_terminals de Tower et 2) dans current_aircraft de Terminal .

Pour le 1), le parcours reserved_terminals afin de trouver l'avion et je l'erase.

Le 2) est plus compliquer, l'avion qui a actuellement reserver le terminal est un champs priver de terminal.
Il faut donc définir une fonction dans terminal qui vas libérer l'avion qui l'a reserver, afin que cela reste interne à la classe.
Je definit donc une fonction : void Terminal::aircraft_crash_free_terminal_spot(Aircraft& aircraft)
qui vas s'occuper de remettre le current_aircraft à nullptr.
Je recupère donc le numéro de terminal qui a était assigner à cette avion dans Tower, puis j'appel cette nouvelle fonction de terminal.
```

### F - Paramétrage (optionnel)

Pour le moment, tous les avions ont la même consommation d'essence (1 unité / trame) et la même taille de réservoir (`3'000`).

1. Arrangez-vous pour que ces deux valeurs soient maintenant déterminées par le type de chaque avion (`AircraftType`).

2. Pondérez la consommation réelle de l'avion par sa vitesse courante.
La consommation définie dans `AircraftType` ne s'appliquera que lorsque l'avion est à sa vitesse maximale.

3. Un avion indique qu'il a besoin d'essence lorsqu'il a moins de `200` unités.
Remplacez cette valeur pour qu'elle corresponde à la quantité consommée en 10s à vitesse maximale.\
Si vous n'avez pas fait la question bonus de TASK_0, notez bien que la fonction `update` de chaque avion devrait être appelée `DEFAULT_TICKS_PER_SEC` fois par seconde. 
