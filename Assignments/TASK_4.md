## Templates

### Objectif 1 - Devant ou derrière ? 

La fonction `Aircraft::add_waypoint` permet de rajouter une étape au début ou à la fin du parcours de l'avion.
Pour distinguer ces deux cas, elle prend un argument booléen `front` (on parle alors de "flag") qui est évalué à l'exécution.
Votre objectif consistera à modifier cette fonction afin d'économiser cette évaluation.  

1. Aujourd'hui, cette fonction n'est pas utilisée dans le code (oups).
Du coup, pour être sûr que vos futurs changements fonctionneront correctement, commencez par modifier le code de la classe `Aircraft` de manière à remplacer :
```cpp
waypoints = control.get_instructions(*this);
```
par
```cpp
auto front = false;
for (const auto& wp: control.get_instructions(*this))
{
   add_waypoint(wp, front);
}
```

2. Modifiez `Aircraft::add_waypoint` afin que l'évaluation du flag ait lieu à la compilation et non à l'exécution.
Que devez-vous changer dans l'appel de la fonction pour que le programme compile ?
```
Il faut utiliser le mot clef constexpr vus dans le chapitre 9.
Et que le flag front devient un paramètre de template et non plus un paramètre de fonction.
```
```cpp
template <bool front> void Aircraft::add_waypoint(const Waypoint& wp)
{
    if constexpr (front)
    {
        waypoints.push_front(wp);
    }
    else
    {
        waypoints.push_back(wp);
    }
}

// When we call it.
...
    if (waypoints.empty())
    {
        // waypoints  = control.get_instructions(*this);
        const auto front = false;
        for (const auto& wp : control.get_instructions(*this))
        {
            add_waypoint<front>(wp);
        }
    }
...
```

3. **BONUS** En utilisant [GodBolt](https://godbolt.org/), comparez le code-assembleur généré par les fonctions suivantes:
<table border="0">
 <tr>
    <td><pre lang="c++">
    int minmax(const int x, const int y, const bool min) {
        return x &lt; y ? (min ? x : y) : (min ? y : x);
    }
    </pre></td>
    <td><pre lang="c++">
    template&lt;bool min&gt;
    int minmax(const int x, const int y){
        return x &lt; y ? (min ? x : y) : (min ? y : x);
    }
    </pre></td>
 </tr>
</table>

### Objectif 2 - Points génériques

1. Reprenez les classes dans `geometry.hpp` et inspirez-vous de `Point2D` et `Point3D` pour définir une unique classe-template `Point` paramétrée par la dimension (nombre de coordonnées) et leur type (entier/float/double).
Pour ce qui est des constructeurs, vous n'ajouterez pour le moment que le constructeur par défaut.
```cpp
template <typename T, size_t dim> class Point
{
public:
    Point() = default;
};
```
```
On précise le typename et la dimension de la classe.
A l'aide du mot clef default, on demande au compilateur de générer le constructeur par défaut, on est donc sur que celui-ci existe.
```

2. Ajoutez une fonction libre `test_generic_points` à votre programme, que vous appelerez depuis le `main`. 
Placez le code suivant dans cette fonction et modifiez-le plusieurs fois, pour vérifier que le compilateur est capable de générer des classes à partir de votre template sans problème :
```cpp
Point<...> p1;
Point<...> p2;
auto p3 = p1 + p2;
p1 += p2;
p1 *= 3; // ou 3.f, ou 3.0 en fonction du type de Point
```

3. Ajoutez le constructeur à 2 paramètres de `Point2D` et le constructeur à 3 paramètres de `Point3D` dans votre classe-template.
Modifiez `Point2D` et `Point3D` afin d'en faire des alias sur des classes générées à partir du template `Point` (respectivement, 2 floats et 3 floats).
Vérifiez que votre programme compile et fonctionne comme avant.
```
Partie du projet très pénible car beaucoup de modification à faire et beaucoup d'erreur à ne pas oublié/gérer.
Liste exhaustif :
    lenght() : ne pas oublié le cast static_cast<T>(0) .
    std::array<T, dim> values; : ne peut pas être priver car il est utiliser dans d'autre fichier tel que waypoints.
    glVertex2fv(vertex.values); : cannot convert ‘const std::array<float, 2>’ to ‘const GLfloat*’ {aka ‘const float*’}
                                    Un enfer pour trouver la solution : Il faut utiliser .datat()
                                    https://en.cppreference.com/w/cpp/container/array/data
```

4. Dans la fonction `test_generic_points`, essayez d'instancier un `Point2D` avec 3 arguments.
Que se passe-t-il ?
Comment pourriez-vous expliquer que cette erreur ne se produise que maintenant ?
```
Point possédent 3 constructeur, default, 2 params et 3 params.
Point2D était en alias à Point, on peut appeler le constructeur à 3 params.
```

5. Que se passe-t-il maintenant si vous essayez d'instancier un `Point3D` avec 2 arguments ?
Utilisez un `static_assert` afin de vous assurez que personne ne puisse initialiser un `Point3D` avec seulement deux éléments.
Faites en de même dans les fonctions `y()` et `z()`, pour vérifier que l'on ne puisse pas les appeler sur des `Point` qui n'ont pas la dimension minimale requise.
```cpp
    static_assert(dim >= 1 && dim <= 3, "Program only support dim 2 and 3.");

    Point() = default;

    // Point2D.
    Point(T x, T y) : values { x, y } { static_assert(dim == 2); }
    // Point3D.
    Point(T x, T y, T z) : values { x, y, z } { static_assert(dim == 3); }
```

6. Plutôt qu'avoir un constructeur pour chaque cas possible (d'ailleurs, vous n'avez pas traité tous les cas possibles, juste 2D et 3D), vous allez utiliser un variadic-template et du perfect-forwarding pour transférer n'importe quel nombre d'arguments de n'importe quel type directement au constructeur de `values`.  
Vous conserverez bien entendu le `static_assert` pour vérifier que le nombre d'arguments passés correspond bien à la dimension du `Point`.\
En faisant ça, vous aurez peut-être désormais des problèmes avec la copie des `Point`.
Que pouvez-vous faire pour supprimer l'ambiguité ?

```
https://en.cppreference.com/w/cpp/language/parameter_pack
https://en.cppreference.com/w/cpp/utility/forward

Dans le fichier texture.hpp, Point2D est appeler avec des int, ce qui pose soucis car Point2D est devenu un alias de Point qui utilise des float, il faut donc rajouter .f pour chaque int.
De même pour runway.hpp; AircraftFactory.cpp; airport_type.hpp; tower_sim.cpp; 
Globalement partout où est appeler l'alias Point3D celui-ci doit prendre des float et non pas des int.

Lorsque l'on utilise std::forward dans un constructeur, il faut préciser en amont du constructeur un template typename du type de ce que l'on veux std::forward.

https://en.cppreference.com/w/cpp/language/sizeof...
sizeof... permet de connaitre la taille des paramètres variadic, je vais donc l'utiliser pour le static_assert.
```

```cpp
    // Solution proposer par Monsieur Marsault.
    Point(std::initializer_list<T> args) //: values { args }
    {
        unsigned i = 0;
        for (T el : args)
            values[i++] = el;
        // std::cout << "constructeur:" << *this << std::endl;
        // static_assert(sizeof...(args...) == dim);
    };
```

```cpp
    // Solution garder.
    template <typename... F> Point(T x, F&&... args) : values { x, std::forward<F>(args)... }
    {
        static_assert(1 + sizeof...(F) == dim);
    };
```

7. **BONUS** En utilisant SFINAE, faites en sorte que le template `Point` ne puisse être instancié qu'avec des types [arithmétiques](https://en.cppreference.com/w/cpp/types/is_arithmetic).
