# Rapport projet cpp GASCHE Luca

Le code est commenté un maximum afin que celui-ci soit le plus compréhensible possible, pour plus de détails il faut consulter les Task.

## Tasks
L'intégralité des questions poser dans les task on une réponse, que celle-ci ai était demander ou non, afin de garder une trace sur chaques modifications apporter au programme accompagné de sa raison et de sont commit associer sur git.

## Choix sur l'archy 

Je n'ai pas réellement fait de choix d'architecture qui diffère de la trame du sujet que j'ai suivi.
J'ai néanmoins quelquefois ajouter des fonctions internet à des classes afin de me "simplifier" la vie, exemple : int Aircraft::get_missing_fuel() const; afin d'éviter la duplication de code.
Chaque ajout que j'ai fait est expliqué dans les Task.

## Situation bloquantes
Ci-dessous la liste des questions que où j'ai bloquer et la solution associer : 

* Task 1 - Objectif 2 - A

```
On commence par déplacer creat_aircraft dans AircraftFactory.
creat_aircraft doit donc changer de signature, celle-ci ne dois plus retourner void mais un unique_ptr<Aicraft>, afin que celui-ci soit ajouter au aircraft_manager.
On retire donc la partie avec l'aircraft_manager de la fonction creat_aircraft.

Trois autres problèmes ce posent à nous dans creat_aircraft, afin crée un nouveau aicraft le constructeur à besoin :
1) AircraftType& type -> Originalement donné en parametre 
2) Tower& control_
3) airlines -> N'est pas définit dans AicraftFactory
Une possibilité est de générer le type d'aicraft dans creat_aircraft.
Pour la Tower, nous pouvons la passer en parametre de la fonction, attention il faut cependant être sur que l'airport est initialiser avant l'appel à create_aircraft : create_aircraft(airport->get_tower()) .
airlines peut être déplacer dans AircraftFactory.hpp .

```

* Task 2 - Objectif 2 - B

```
Gros problème qui m'a pris beaucoup de temps à résoudre :
1) Dans la méthode Aircraft::move(), on appelle comme demander `Tower::reserve_terminal` qui retourne un WaypointQueue.
Ce WaypointQueue contient le nouveau chemin de l'avion, il faut donc l'assigner à l'ancien.
Il se pose donc un souci qui est que l'opérateur d'assignation pour un WaypointQueue n'est pas défini, il faut donc le faire.
2) Une fois cette opération définit, dans airpot_type.hpp lorsuqu'un WaypointQueue est créer celui ci nous dit : 
    implicitly-declared is deprecated 
    WaypointQueue result { before_in_air, runway_middle, runway_end, crossing };
Il faut donc définir un constructeur de copie par default.
```

* Task 2 - Objectif 2 - D
```
Dans la méthode Terminal::refill_aircraft_if_needed(int& fuel_stock) , ne pas oublier de vérifier si le terminal à actuellement un aircraft (current_aircraft != nullptr).
Car dans Airport::update nous allons l'appeler sur chaque terminaux, donc si un des terminaux n'a actuellement pas d'avion assigné alors un segfault ve déclenché.
```

* Task 2 - Objectif 2 - E

```
Dans Tower je définis une nouvelle fonction qui va remplir le rôle demander : Tower::aircraft_crash_free_terminal_spot(Aircraft& aircraft) .
Un souci c'est poser à moi, lorsque l'avion effectue sa réservation (si celle-ci est approuvée) sera présent dans deux partie du programme : 1) dans reserved_terminals de Tower et 2) dans current_aircraft de Terminal .

Pour le 1), le parcours reserved_terminals afin de trouver l'avion et je l'erase.

Le 2) est plus compliqué, l'avion qui a actuellement réserver le terminal est un champ privé de terminal.
Il faut donc définir une fonction dans terminal qui va libérer l'avion qui l'a réservé, afin que cela reste interne à la classe.
Je définis donc une fonction : void Terminal::aircraft_crash_free_terminal_spot(Aircraft& aircraft)qui va s'occuper de remettre le current_aircraft à nullptr.Je récupère donc le numéro de terminal qui a été assigné à cet avion dans Tower, puis j'appelle cette nouvelle fonction de terminal.
```

* Task 4 - Objectif 2

```
Partie du projet très dur car beaucoup de modifications à faire et beaucoup d'erreurs à ne pas oublier/gérer.
Liste exhaustive :
    lenght() : ne pas oublié le cast static_cast<T>(0) .
    std::array<T, dim> values; : ne peut pas être privé car il est utilisé dans d'autres fichiers tel que waypoints.
    glVertex2fv(vertex.values); : cannot convert ‘const std::array<float, 2>’ to ‘const GLfloat*’ {aka ‘const float*’}
                                    Un enfer pour trouver la solution : Il faut utiliser .datat()
                                    https://en.cppreference.com/w/cpp/container/array/data

```

## Aimé et/ou détesté

Le projet était intéressant et visuel, les consignes plutôt claires et bien découpées selon les différents chapitres vus au cours du développement/avancé du projet.

Appréciant le C j'ai pris du plaisir à réaliser ce projet.

La partie sur les templates était vraiment dure, surtout la dernière question (avis personnel).

Le fait de reprendre un projet déjà existant codé par d'autres personnes, et vraiment un exercice pas facile. Il faut se plonger dans tous les fichiers, réfléchir et s'interroger en se demandant : "Pourquoi cela a été implémenté de cette manière ?" .
Heureusement pour nous le code était commenté !

## Appris ?

Comme dit dans le point précédent, le fait de se plonger dans le code d'autres personnes n'est pas évident.
Ce projet est donc une bonne initiative il permet de nous donner une première expérience sur cette manière de travailler, car une fois diplômé nous serons plutôt dans le maintien de projets qui ont plusieurs années, que dans la conception de nouveaux projets.

De plus la découpe du projet et la trame selon les chapitres vus pendant l'année à était bien réaliser, c'était un bon projet pour apprendre le cpp.