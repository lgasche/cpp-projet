#pragma once

#include "AircraftFactory.hpp"
#include "AircraftManager.hpp"

class Airport;
struct AircraftType;

class TowerSimulation
{
private:
    bool help        = false;
    Airport* airport = nullptr;
    AircraftManager aircraft_manager;
    AircraftFactory aircraft_factory;

    TowerSimulation(const TowerSimulation&) = delete;
    TowerSimulation& operator=(const TowerSimulation&) = delete;

    void create_random_aircraft();

    void create_keystrokes();
    void display_help() const;
    void display_airline(int num_airline) const;

    void init_airport();

    void display_info_fuel() const;

public:
    TowerSimulation(int argc, char** argv);
    ~TowerSimulation();

    void launch();
};
