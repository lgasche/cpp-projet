#pragma once

#include "aircraft_types.hpp"

class Aircraft;
class Tower;

class AircraftFactory
{

public:
    std::unique_ptr<Aircraft> create_aircraft(Tower& tower_control);
    std::string airlines[8] = { "AF", "LH", "EY", "DL", "KL", "BA", "AY", "EY" };

private:
    std::string new_flight_numbers();

    std::unordered_set<std::string> flight_numbers;
};