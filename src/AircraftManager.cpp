#include "AircraftManager.hpp"

#include <numeric>

void AircraftManager::move()
{
    // Part sorts aircrafts by booking terminal and quantity of fuel.
    const auto lambda_sort_by_less_fuel_and_terminal_reserved = [](auto& a, auto& b)
    {
        // Check by terminal reserved.
        if (a->has_terminal() != b->has_terminal())
        {
            return a->has_terminal();
        }
        // Check by quantity of fuel.
        return a->get_fuel() < b->get_fuel();
    };
    std::sort(aircrafts.begin(), aircrafts.end(), lambda_sort_by_less_fuel_and_terminal_reserved);

    // Part move all aircrafts.
    const auto lambda_update_aircraft = [this](auto& aircraft)
    {
        try
        {
            aircraft->move();
            return aircraft->must_destroyed();
        } catch (const AircraftCrash& err)
        {
            std::cerr << err.what() << std::endl;
            aircraft_crash_count++;
            return true;
        }
    };
    aircrafts.erase(std::remove_if(aircrafts.begin(), aircrafts.end(), lambda_update_aircraft),
                    aircrafts.end());
}

void AircraftManager::add_aircraft(std::unique_ptr<Aircraft> aircraft)
{
    assert(aircraft != nullptr);
    aircrafts.emplace_back(std::move(aircraft));
}

int AircraftManager::count_aircraft_current_on_airline(const std::string_view& airline) const
{
    const auto lambda = [airline](const auto& aircraft)
    { return (aircraft->get_flight_num().find(airline) == 0); };

    return std::count_if(aircrafts.begin(), aircrafts.end(), lambda);
}

void AircraftManager::display_all_aircrafts_info_fuel() const
{
    std::cout << "__________START_INFO_FUEL__________" << std::endl;
    for (auto it = aircrafts.cbegin(); it != aircrafts.cend(); it++)
    {
        (*it)->display_info_fuel();
    }
    std::cout << "______________END__________________" << std::endl;
};

int AircraftManager::get_required_fuel() const
{
    const auto lambda = [](int total_required_fuel, const auto& aircraft)
    {
        return aircraft->is_circling() && aircraft->is_low_on_fuel()
                   ? total_required_fuel + aircraft->get_missing_fuel()
                   : total_required_fuel;
    };

    return std::accumulate(aircrafts.cbegin(), aircrafts.cend(), 0, lambda);
}

void AircraftManager::display_info_crash() const
{
    std::cout << "Total aircrafts crash : " << aircraft_crash_count << std::endl;
}