#include "tower_sim.hpp"

void test_generic_points()
{
    std::cout << "debut" << std::endl;

    // Case +
    auto p1 = Point3D { 1.f, 2.f, 3.f };
    auto p2 = Point3D { 1.f, 2.f, 3.f };
    std::cout << "Case + : "
              << "\n"
              << p1 << "\n"
              << p2 << "\n"
              << p1 + p2 << std::endl;

    // Case -
    std::cout << "Case - : "
              << "\n"
              << p1 << "\n"
              << p2 << "\n"
              << p1 - p2 << std::endl;

    // Case * with Point
    std::cout << "Case * with Point : "
              << "\n"
              << p1 << "\n"
              << p2 << "\n"
              << p1 * p2 << std::endl;

    // Case * with scalar
    std::cout << "Case * with scalar : "
              << "\n"
              << p1 << "\n"
              << 2 << "\n"
              << p1 * 2 << std::endl;

    // Case lenght
    std::cout << "Case lenght : "
              << "\n"
              << p1 << "\n"
              << "\n"
              << (-p1).length() << std::endl;

    // Case normalize
    std::cout << "Case normalize : "
              << "\n"
              << p1 << "\n"
              << "\n"
              << p1.normalize() << std::endl;

    // Case normalize with p
    std::cout << "Case normalize with p : "
              << "\n"
              << p1 << "\n"
              << "\n"
              << (-p1).normalize() << std::endl;
}

int main(int argc, char** argv)
{
    TowerSimulation simulation { argc, argv };
    simulation.launch();

    // test_generic_points();

    return 0;
}