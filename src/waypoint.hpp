#pragma once

#include "geometry.hpp"

#include <deque>

enum WaypointType
{
    wp_air,
    wp_ground,
    wp_terminal
};

class Waypoint : public Point3D
{
public:
    WaypointType type;

    Waypoint(const Point3D& position, const WaypointType type_ = wp_air) :
        Point3D { position }, type { type_ }
    {}

    Waypoint(const Waypoint&) = default;

    bool is_on_ground() const { return type != wp_air; }
    bool is_at_terminal() const { return type == wp_terminal; }

    Waypoint& operator=(const Waypoint& other)
    {
        if (this != &other)
        {
            // invalid array assignment
            values = other.values;
            // assignment of read-only member ‘Waypoint::type’
            // remove the const to type.
            type = other.type;
        }
        return *this;
    }
};

using WaypointQueue = std::deque<Waypoint>;