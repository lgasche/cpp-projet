#pragma once

#include "GL/dynamic_object.hpp"
#include "aircraft.hpp"

#include <memory>
#include <vector>

class Aicraft;

class AircraftManager : public GL::DynamicObject
{

public:
    //    AircraftManager(); // Constructor.
    //    ~AircraftManager();
    void move() override;
    void add_aircraft(std::unique_ptr<Aircraft>);
    int count_aircraft_current_on_airline(const std::string_view& airline) const;
    void display_all_aircrafts_info_fuel() const;
    int get_required_fuel() const;
    void display_info_crash() const;

private:
    std::vector<std::unique_ptr<Aircraft>> aircrafts;

    int aircraft_crash_count = 0;
};
