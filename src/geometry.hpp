#pragma once

#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <iostream>
#include <numeric>

template <size_t dim, typename T> class Point;
using Point2D = Point<2, float>;
using Point3D = Point<3, float>;

template <size_t dim, typename T> std::ostream& operator<<(std::ostream& stream, const Point<dim, T>& point);

template <size_t dim, typename T> class Point
{
    static_assert(dim >= 1 && dim <= 3, "Program only support dim 2 and 3.");

public:
    std::array<T, dim> values;

    Point() = default;
    Point(const Point& other) : values { other.values } {}

    /*
    Point(std::initializer_list<T> args) //: values { args }
    {
        unsigned i = 0;
        for (T el : args)
            values[i++] = el;
        // std::cout << "constructeur:" << *this << std::endl;
        // static_assert(sizeof...(args...) == dim);
    };
    */

    template <typename... F> Point(T x, F&&... args) : values { x, std::forward<F>(args)... }
    {
        static_assert(1 + sizeof...(F) == dim);
    };

    template <size_t dim2, typename T2>
    friend std::ostream& operator<<(std::ostream& stream, const Point<dim2, T2>& point);

    T& x() { return std::get<0>(values); }
    T x() const { return std::get<0>(values); }

    T& y()
    {
        static_assert(dim >= 2);
        return std::get<1>(values);
    }
    T y() const
    {
        static_assert(dim >= 2);
        return std::get<1>(values);
    }

    T& z()
    {
        static_assert(dim >= 3);
        return std::get<2>(values);
    }
    T z() const
    {
        static_assert(dim >= 3);
        return std::get<2>(values);
    }

    Point& operator+=(const Point& other)
    {
        std::transform(values.cbegin(), values.cend(), other.values.cbegin(), values.begin(), std::plus<T>());
        return *this;
    }

    Point& operator-=(const Point& other)
    {
        std::transform(values.cbegin(), values.cend(), other.values.cbegin(), values.begin(),
                       std::minus<T>());
        return *this;
    }

    // Use in Point2D.
    Point& operator*=(const Point& other)
    {
        std::transform(values.cbegin(), values.cend(), other.values.cbegin(), values.begin(),
                       std::multiplies<T>());
        return *this;
    }

    Point& operator*=(const T scalar)
    {
        std::transform(values.cbegin(), values.cend(), values.begin(),
                       ([scalar](T n) { return n * scalar; }));
        return *this;
    }

    Point operator+(const Point& other) const
    {
        Point result = *this;
        result += other;
        return result;
    }

    Point operator-(const Point& other) const
    {
        Point result = *this;
        result -= other;
        return result;
    }

    Point operator*(const T scalar) const
    {
        Point result = *this;
        result *= scalar;
        return result;
    }

    // Use in Point2D.
    Point operator*(const Point& other) const
    {
        Point result = *this;
        result *= other;
        return result;
    }

    Point operator-() const
    {
        Point result;
        result -= *this;
        return result;
    }

    T length() const
    {
        return std::sqrt(std::accumulate(values.begin(), values.end(), static_cast<T>(0),
                                         [](T sum, T n) { return sum + (n * n); }));
    }

    T distance_to(const Point& other) const { return (*this - other).length(); }

    Point& normalize(const T target_len = 1.0f)
    {
        const T current_len = length();
        if (current_len == 0)
        {
            throw std::logic_error("cannot normalize vector of length 0");
        }

        *this *= (target_len / current_len);
        return *this;
    }

    Point& cap_length(const T max_len)
    {
        assert(max_len > 0);

        const T current_len = length();
        if (current_len > max_len)
        {
            *this *= (max_len / current_len);
        }

        return *this;
    }
};

template <size_t dim, typename T> std::ostream& operator<<(std::ostream& stream, const Point<dim, T>& point)
{
    for (const T& e : point.values)
    {
        stream << e << " ";
    }

    return stream << std::endl;
}

// our 3D-coordinate system will be tied to the airport: the runway is parallel to the x-axis, the z-axis
// points towards the sky, and y is perpendicular to both thus,
// {1,0,0} --> {.5,.5}   {0,1,0} --> {-.5,.5}   {0,0,1} --> {0,1}
inline Point2D project_2D(const Point3D& p)
{
    return { .5f * p.x() - .5f * p.y(), .5f * p.x() + .5f * p.y() + p.z() };
}
