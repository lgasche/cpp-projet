#include "tower_sim.hpp"

#include "AircraftFactory.hpp"
#include "GL/opengl_interface.hpp"
#include "aircraft.hpp"
#include "airport.hpp"
#include "config.hpp"
#include "img/image.hpp"
#include "img/media_path.hpp"

#include <cassert>
#include <cstdlib>
#include <ctime>
#include <utility>

using namespace std::string_literals;

TowerSimulation::TowerSimulation(int argc, char** argv) :
    help { (argc > 1) && (std::string { argv[1] } == "--help"s || std::string { argv[1] } == "-h"s) }
{
    MediaPath::initialize(argv[0]);
    std::srand(static_cast<unsigned int>(std::time(nullptr)));
    GL::init_gl(argc, argv, "Airport Tower Simulation");
    aircraft_manager = AircraftManager();
    GL::move_queue.emplace(&aircraft_manager);
    aircraft_factory = AircraftFactory();

    create_keystrokes();
}

TowerSimulation::~TowerSimulation()
{
    delete airport;
}

void TowerSimulation::create_random_aircraft()
{
    assert(airport); // Make sure the airport is initialized before creating aircraft.
    aircraft_manager.add_aircraft(aircraft_factory.create_aircraft(airport->get_tower()));
}

void TowerSimulation::create_keystrokes()
{
    GL::keystrokes.emplace('x', []() { GL::exit_loop(); });
    GL::keystrokes.emplace('q', []() { GL::exit_loop(); });
    GL::keystrokes.emplace('c', [this]() { create_random_aircraft(); });
    GL::keystrokes.emplace('+', []() { GL::change_zoom(0.95f); });
    GL::keystrokes.emplace('-', []() { GL::change_zoom(1.05f); });
    GL::keystrokes.emplace('f', []() { GL::toggle_fullscreen(); });
    GL::keystrokes.emplace('p', []() { GL::pause(); });
    GL::keystrokes.emplace('m', []() { GL::more_framerate(); });
    GL::keystrokes.emplace('l', []() { GL::less_framerate(); });
    for (int i = 0; i < 8; i++)
    {
        GL::keystrokes.emplace('0' + i, [this, i]() { display_airline(i); });
    }
    GL::keystrokes.emplace('i', [this]() { display_info_fuel(); });
    GL::keystrokes.emplace('k', [this]() { aircraft_manager.display_info_crash(); });
}

void TowerSimulation::display_airline(int num_airline) const
{
    assert(num_airline <= 7);
    const auto& airline = aircraft_factory.airlines[num_airline];
    const int n         = aircraft_manager.count_aircraft_current_on_airline(airline);
    std::cout << n << " aircrafts currently on the airline " << airline << std::endl;
}

void TowerSimulation::display_info_fuel() const
{
    aircraft_manager.display_all_aircrafts_info_fuel();
};

void TowerSimulation::display_help() const
{
    std::cout << "This is an airport tower simulator" << std::endl
              << "the following keysstrokes have meaning:" << std::endl;

    for (auto& [key, value] : GL::keystrokes)
    {
        std::cout << key << ' ';
    }

    std::cout << std::endl;
}

void TowerSimulation::init_airport()
{
    airport =
        new Airport { one_lane_airport, Point3D { 0.f, 0.f, 0.f },
                      new img::Image { one_lane_airport_sprite_path.get_full_path() }, aircraft_manager };

    GL::move_queue.emplace(airport);
}

void TowerSimulation::launch()
{
    if (help)
    {
        display_help();
        return;
    }

    init_airport();
    init_aircraft_types();

    GL::loop();
}
