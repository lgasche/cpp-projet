#include "AircraftFactory.hpp"

#include "aircraft.hpp"

std::unique_ptr<Aircraft> AircraftFactory::create_aircraft(Tower& tower_control)
{
    const std::string flight_number = new_flight_numbers();
    const float angle   = (rand() % 1000) * 2 * 3.141592f / 1000.f; // random angle between 0 and 2pi
    const Point3D start = Point3D { std::sin(angle), std::cos(angle), 0.f } * 3 + Point3D { 0.f, 0.f, 2.f };
    const Point3D direction  = (-start).normalize();
    const AircraftType& type = *(aircraft_types[rand() % 3]);

    return std::make_unique<Aircraft>(type, flight_number, start, direction, tower_control);
}

std::string AircraftFactory::new_flight_numbers()
{
    std::string flight_number;
    do
    {
        flight_number = airlines[std::rand() % 8] + std::to_string(1000 + (rand() % 9000));
    }
    while (flight_numbers.find(flight_number) != flight_numbers.end());
    flight_numbers.emplace(flight_number);
    return flight_number;
}