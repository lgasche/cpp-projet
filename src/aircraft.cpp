#include "aircraft.hpp"

#include "GL/opengl_interface.hpp"

#include <cmath>

void Aircraft::turn_to_waypoint()
{
    if (!waypoints.empty())
    {
        Point3D target = waypoints[0];
        if (waypoints.size() > 1)
        {
            const float d   = (waypoints[0] - pos).length();
            const Point3D W = (waypoints[0] - waypoints[1]).normalize(d / 2.0f);
            target += W;
        }

        turn(target - pos - speed);
    }
}

void Aircraft::turn(Point3D direction)
{
    (speed += direction.cap_length(type.max_accel)).cap_length(max_speed());
}

unsigned int Aircraft::get_speed_octant() const
{
    const float speed_len = speed.length();
    if (speed_len > 0)
    {
        const Point3D norm_speed { speed * (1.0f / speed_len) };
        const float angle =
            (norm_speed.y() > 0) ? 2.0f * 3.141592f - std::acos(norm_speed.x()) : std::acos(norm_speed.x());
        // partition into NUM_AIRCRAFT_TILES equal pieces
        return (static_cast<int>(std::round((angle * NUM_AIRCRAFT_TILES) / (2.0f * 3.141592f))) + 1) %
               NUM_AIRCRAFT_TILES;
    }
    else
    {
        return 0;
    }
}

/**
 * When the aircraft arrive at a terminal, signal the tower.
 */
void Aircraft::arrive_at_terminal()
{
    assert(is_at_terminal == false);
    assert(visited_a_terminal == false);

    // Arrived at a terminal, so start servicing.
    control.arrived_at_terminal(*this);
    is_at_terminal     = true;
    visited_a_terminal = true;
}

// deploy and retract landing gear depending on next waypoints
void Aircraft::operate_landing_gear()
{
    if (waypoints.size() > 1u)
    {
        const auto it            = waypoints.begin();
        const bool ground_before = it->is_on_ground();
        const bool ground_after  = std::next(it)->is_on_ground();
        // deploy/retract landing gear when landing/lifting-off
        if (ground_before && !ground_after)
        {
            std::cout << flight_number << " lift off" << std::endl;
        }
        else if (!ground_before && ground_after)
        {
            std::cout << flight_number << " is now landing..." << std::endl;
            landing_gear_deployed = true;
        }
        else if (!ground_before && !ground_after)
        {
            landing_gear_deployed = false;
        }
    }
}

template <bool front> void Aircraft::add_waypoint(const Waypoint& wp)
{
    if constexpr (front)
    {
        waypoints.push_front(wp);
    }
    else
    {
        waypoints.push_back(wp);
    }
}

void Aircraft::move()
{
    update_fuel();

    if (is_circling() && !this->visited_a_terminal)
    {
        // Tries to reserve a terminal and retrieves the associated path,
        // if not available path is empty.
        auto new_waypoints = control.reserve_terminal(*this);
        if (!new_waypoints.empty())
        {
            // Update the path.
            waypoints = new_waypoints;
        }
    }

    if (waypoints.empty())
    {
        // waypoints  = control.get_instructions(*this);
        const auto front = false;
        for (const auto& wp : control.get_instructions(*this))
        {
            add_waypoint<front>(wp);
        }
    }

    if (!is_at_terminal)
    {
        turn_to_waypoint();
        // move in the direction of the current speed
        pos += speed;

        // if we are close to our next waypoint, stike if off the list
        if (!waypoints.empty() && distance_to(waypoints.front()) < DISTANCE_THRESHOLD)
        {
            if (waypoints.front().is_at_terminal())
            {
                arrive_at_terminal();
            }
            else
            {
                operate_landing_gear();
            }
            waypoints.pop_front();
        }

        if (is_on_ground())
        {
            if (!landing_gear_deployed)
            {
                using namespace std::string_literals;
                throw AircraftCrash { flight_number + " crashed into the ground"s };
            }
        }
        else
        {
            // if we are in the air, but too slow, then we will sink!
            const float speed_len = speed.length();
            if (speed_len < SPEED_THRESHOLD)
            {
                pos.z() -= SINK_FACTOR * (SPEED_THRESHOLD - speed_len);
            }
        }

        // update the z-value of the displayable structure
        GL::Displayable::z = pos.x() + pos.y();
    }
}

void Aircraft::display() const
{
    type.texture.draw(project_2D(pos), { PLANE_TEXTURE_DIM, PLANE_TEXTURE_DIM }, get_speed_octant());
}

bool Aircraft::must_destroyed()
{
    // Case aircraft is far destroyed.
    if (visited_a_terminal == true && is_on_ground() == false && waypoints.empty())
    {
        std::cout << flight_number << " is destroyed." << std::endl;
        return true;
    }

    // Case aircraft have no more fuel crash.
    else if (fuel == 0 && !is_on_ground())
    {
        auto err = flight_number + " is crash.";
        throw AircraftCrash { err };
    }
    else
    {
        return false;
    }
}

void Aircraft::update_fuel()
{
    if (fuel > 0)
    {
        if (!is_on_ground())
        {
            fuel--;
        }
    }
}

bool Aircraft::has_terminal() const
{
    return !waypoints.empty() && waypoints.back().is_at_terminal();
}

bool Aircraft::is_circling() const
{
    return !waypoints.empty() && !waypoints.back().is_on_ground() && !landing_gear_deployed;
}

int Aircraft::get_missing_fuel() const
{
    return 3000 - fuel;
}

void Aircraft::refill(int& fuel_stock)
{
    // Can't be negative.
    if (fuel_stock == 0)
    {
        return;
    }

    auto missing_fuel = get_missing_fuel();
    // If more fuel is missing than the stock fuel, then take it all.
    if (missing_fuel > fuel_stock)
    {
        fuel += fuel_stock;
        fuel_stock = 0;
        std::cout << flight_number << " - Refuelling : " << fuel_stock << " -> Aircraft not full."
                  << std::endl;
    }
    // Else take only the missing fuel.
    else
    {
        fuel += missing_fuel;
        fuel_stock -= missing_fuel;
        std::cout << flight_number << " - Refuelling : " << missing_fuel << " -> Aircraft full." << std::endl;
    }
}